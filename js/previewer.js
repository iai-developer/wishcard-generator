(function ($){

        // fonts
    police = [
        "f1",
        "f2",
        "f3",
        "f4",
        "f5",
        "f6",
        "f7",
        "f8",
        "f9",
        "f10",
        "f11",
        "f12",
        "f13",
        "f14",
        "f15",
    ];

    var i = 0;
    police.forEach(element => {
        i++;
        name = "Wish-card Font "+i;
        $('#font-preview').append("<option class='"+element+"' value='"+ element +"' data-class='"+element+"'>"+name+"</option>");
    });

        // backgrouds

    background = [
        "00.jpg",
        "11.jpg",
        "22.jpg",
        "33.jpg",
        "44.jpg",
        "55.jpg",
        "66.jpg",
        "77.jpg",
        "88.jpg",
        "99.jpg",
        "111.jpg",
        "112.jpg",
        "113.jpg",
        "114.jpg",
    ];
    var i = 1;
    background.forEach(back => {
        $("#bg").append("<option value='images/bg/"+back+"' data-icon='images/bg/"+back+"' class='left'> Arriere plan "+i+"</option>");
        i++;
    });

    $("#bg").change(function(){
        var background = $(this).val();
        $(".card-preview-bg").css({
            "background-image": "url("+background+")"
        });
    });

    // upload background picture

    document.getElementById('getbg').addEventListener('change', readURL, true);
      function readURL(){
          var file = document.getElementById('getbg').files[0];
          var render = new FileReader();
          render.onloadend = function(){
              document.getElementById('card-bg').style.backgroundImage = "url("+render.result+")";
              $('#card-bg').css('background-size','cover');
          }
          if(file){
              render.readAsDataURL(file);
          }else{
          }
      }


      // logos

    logos = [
        "00.png",
        "11.png",
        "22.png",
        "33.png",
        "44.png",
        "55.png",
        "66.png",
        "77.png",
        "88.png",
        "99.png",
        "111.png",
        "112.png",
        "113.png",
    ];
    var i = 1;
    logos.forEach(logo => {
        $("#sq").append("<option value='images/sq/"+logo+"' data-icon='images/sq/"+logo+"' class='left'>logo "+i+"</option>");
        i++;
    });
  
    // change logo

    $("#sq").change(function(){
        var background = $(this).val();
        $(".card-preview-sq").css({
            "background-image": "url("+background+")"
        });
    });

    // live text update

    $("#message").keyup(function(){
        var message = $(this).val();
        $('.card-preview-message').text(message);
    });
7
    $("#title").keyup(function(){
        var title = $(this).val();
        $('.card-preview-title').text(title);
    });

    $("#wish").keyup(function(){
        var wish = $(this).val();
        $('.card-preview-wish').text(wish);
    });

    $("#to-chooser").change(function(){
        switch ($("#to-chooser").val()) {
        case "maman":
            var to = "A ma maman chérie,";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;

        case "papa":
            var to = "A mon super papa,";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;

        case "petitami":
            var to = "A toi mon Amour,";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;

        case "petiteamie":
            var to = "A toi mon coeur,";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;

        case "autre":
            var to = "";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;

        case "ami":
            var to = "A mon ami(e),";
            $('.card-preview-to').text(to);
            $("#to").val(to);
            break;
        };
    });

    $("#to").keyup(function(){
        var to = $(this).val();
        $('.card-preview-to').text(to);
    }); 

    $("#font-preview").change(function(){
        var font = $(this).val();
        $("#wiltek").removeClass().addClass(font);
    });

    $(document).ready(function (e) {
        
        $("#color-preview").on("change.preview.color", function (c) {
            var color = $(c.target).children("option").eq(c.target.selectedIndex).val();
            var $textEl = $(".card-preview-to, .card-preview-title, .card-preview-message, .card-preview-wish");

            $textEl.css({
                color: "rgb(" + color + ")"
            });
        });
        $('#color-picker-component').colorpicker().on('changeColor.colorpicker', function (event) {
            var $textEl = $(".card-preview-to, .card-preview-title, .card-preview-message, .card-preview-wish");

            $textEl.css({
                color: event.color.toHex()
            });
        });
        $("#font-size-range").on("change.preview.size", function (c) {
            var $textEl = $(".card-preview-to, .card-preview-title, .card-preview-message, .card-preview-wish");

            $textEl.css({
                "font-size": $(c.target).val() + "px"
            });
        });
    });



    // to make a screenshoot of the wish-card Div
    $('#card-generator-btn').click(function(){
        $("#card-generator-preloader").removeClass("hide");
        $(".dev-signature").removeClass('hide').css('font-family','arial').css('font-size', "15px");

        div_content = document.getElementById("card-bg");
        //make it as html5 canvas
        html2canvas(div_content).then(function(canvas) {
            //change the canvas to jpeg image
            data = canvas.toDataURL('image/jpeg');
            
            //then call a super hero php to save the image
            save_img(data);
        });
    });

    //to save the canvas image on the computer
    function save_img(data){
        //ajax method.
        $.post('save_jpg.php', {data: data}, function(res){
            //if the file saved properly, trigger a popup to the user.
            if(res != ''){
                $("#card-generator-preloader").addClass("hide");
                $("#card-generated-image").removeClass('hide');
                $("#card-generated").attr("src", "images/generated/" + res + ".jpg");
                $(".dev-signature").addClass('hide');
                $("#link-share-facebook").attr("href", "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2F127.0.0.1:5555/images/generated/"+res+".jpg");
                $("#link-share-whatsapp").attr("href", "whatsapp://send?text=http://192.168.43.167:5555/images/generated/"+res+".jpg");
                    
            // 	yes = confirm('Image enregistré avec succes. cliquez sur ok pour visualiser  '+res);
            // 	if(yes){
            // 		location.href =document.URL+'/images/generated/'+res+'.jpg';
            // 	}
            // }
            // else{
            // 	alert('something wrong');
            }
        });
    }
})(jQuery);