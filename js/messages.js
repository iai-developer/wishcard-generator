$(document).ready(function () {

    // card's templates
    for (var i = 0; i < template.length; i++) {
        sample = "<article> <div class='sample card image card-model' id='"+i+"'> <img src='./images/samples/"+template[i].sample+"' alt='' class='waves-effect waves-light'/> </div> </article>";
        $("#samples").append(sample);
    }

    $('.sample').click(function(){
        var id = this.id;

        var background ="images/bg/"+template[id].bg;
        var logo = "images/sq/"+template[id].logo;
        var wish = template[id].Fwish;
        var message = template[id].Fmessage;
        var to = template[id].to;
        var title = "Pour cette fête de fin d'année";

        $('#to').val(to);
        $('#message').val(message);
        $('#wish').val(wish);  
        $('#title').val(title);


        $('.card-preview-to').text(to);
        $('.card-preview-title').text(title);
        $('.card-preview-message').text(message);
        $('.card-preview-wish').text(wish);
        $(".card-preview-sq").css({
            "background-image": "url("+logo+")"
        });
        $(".card-preview-bg").css({
            "background-image": "url("+background+")"
        });
        var textEl = $(".card-preview-to, .card-preview-title, .card-preview-message, .card-preview-wish");
        textEl.css('color',template[id].color).css('font-size',template[id].fontSize);
        $("#wiltek").removeClass().addClass(template[id].font);
        $("#begin").click();
        // window.location.href = "#card-editor";
    });

    function randomMessageGen(person, feast) {
        var posDisplayMsg = [];
        var pos;
        if(personn = null) return;
        for (var i = 0; i < person.length; i++) {
            if (person[i].type == feast) {
                posDisplayMsg.push(i);
            }
        }

        var pos = Math.floor(Math.random() * (posDisplayMsg.length));
        var currentMessage = $("#message").val();
        if(currentMessage == person[pos].Fmessage) randomMessageGen(person, feast);
        
        $('#message').val(person[pos].Fmessage);
        var message = $("#message").val();
        $('.card-preview-message').text(message);
        title = "pour cette fête de fin d'année";
        $('#title').text(title);
        $('.card-preview-title').text(title);

        //    var pos = Math.floor(Math.random() * (person.length));

    }

    $("#refresh").click(function () {
        switch ($("#to-chooser").val()) {
            case "maman":
                randomMessageGen(maman, "Noel");

                break;

            case "papa":
                randomMessageGen(papa, "Noel");
                break;

            case "petitami":
                randomMessageGen(petitami, "Noel");
                break;

            case "petiteamie":
                randomMessageGen(petiteamie, "Noel");
                break;

            case "autre":
                randomMessageGen(autre, "Noel");
                break;

            case "ami":
                randomMessageGen(ami, "Noel");
                break;
        }

    });

    $("#to-chooser").change(function () {
        switch ($("#to-chooser").val()) {
            case "maman":
                randomMessageGen(maman, "Noel");

                break;

            case "papa":
                randomMessageGen(papa, "Noel");
                break;

            case "petitami":
                randomMessageGen(petitami, "Noel");
                break;

            case "petiteamie":
                randomMessageGen(petiteamie, "Noel");
                break;

            case "autre":
                randomMessageGen(autre, "Noel");
                break;

            case "ami":
                randomMessageGen(ami, "Noel");
                break;
        }

    });




});
