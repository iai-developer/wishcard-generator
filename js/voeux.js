

var papa = [
    {
        number: 1,
        Fmessage: "Mon papa, les fêtes de fin d'année nous rappellent l'amour de ce que l'on aime... Papa adoré, je voudrais te souhaiter un Joyeux Noël et te présenter mes voeux de Bonne Année...",
        Emessage: "My dear father for this end of year, i wish to thank you a Happy Christmas",
        type: "Noel "
    },
    {
        number: 2,
        Fmessage: "Je t'aime mon tendre père, tu es le plus gentil et le plus tendre des papas du monde...  Que le nouvel an soit pour toi, mon papa chérie et pour ma maman chérie le début d'une nouvelle vie de tendresse, de bonheur... Que la santé vous accompagne durant toute l'année Nouvelle...",
        Emessage: "My dear Daddy, thanks for all the love and care, wishing you all the best for this new year",
        type: "Nouvelan"
    },
    {
        number: 3,
        Fmessage: "Je t'aime papa..." + " Joyeux Noël et bonne année de la part d'un enfant au meilleur des pères: Mon Papa d'amour",
        Emessage: "My dear Daddy for this end of year, I wish you a happy end of year and Happy Christmas",
        type: "Noel"

    },
    {
        number: 4,
        Fmessage: "Meilleurs voeux de bonheur à mon père que j'aime d'un amour éternel! Papa je te souhaite le meilleur de l'existence: l'amour de ta famille, l'amitié sincère de tes amis et surtout une bonne santé pour profiter pleinement des bonheurs quotidien! Ton fils qui t'aime avec respect et estime.",
        Emessage: "All my best wishes for this new upcoming year, I love you my dear Daddy",
        type: "Nouvelan"
    },
    {
        number: 5,
        Fmessage: " Pendant de nombreuses années, tu m'as guidé sur Ce chemin que l'on appelle la vie. Les obstacles,"+
        "Les luttes, les déceptions, n'ont pas été difficile"+
        "A combattre avec toi à mes côtés. Je souhaitais donc"+
        "Profiter noél pour te remercier d'être"+
        "Le père que tu es ! Bonne année papa !",
        Emessage: " My daddy this end of year is an occasion for me to thank you for all the love you have been showing me and all the encouragements received from you, if i am what i am today is thanks to you.\n"+ " My dear Daddy for this new year i wish you a long life, good health and much more wealth.\n"
          +"Happy New Year",
        type: "Nouvelan"
    },
    {
        number: 6,
        Fmessage: " N’y a pas mieux que toi Ya pas plus merveilleux. Avec toi comme papa, Ya pas d'ennuis à se faire Y a toute la joie. Papa, Tout ça pour te dire une seul chose Je t’aime et ça ne changera jamais ! Bonne année ",
        Emessage: " For this New year i wish to thank you for the support i received... You have always been a model for me, an advicer and my source of inspiration. For this year, i pray God to give you the force and health",
        type: "Nouvelan"
    },
    {
        number: 7,
        Fmessage: "Gentil, attentionné, tendre, patient, cultivé, sage, drôle, rassurant… Dans cette carte, je souhaiterais réunir toutes les qualités du dictionnaire pour te les dédicacer ! Parce que tu es, tout simplement, le plus formidable des papas du monde ! Bonne fête mon papa :o)",
        Emessage: "My best wishes to my lovely father that i love with all my heart! Daddy I wish you all the best for your existence: love, peace, joy and wealth",
        type: "Nouvelan"
    },
    {
        number: 8,
        Fmessage: "Une petite carte pour te rendre hommage, mon cher père. Sache que, même si je ne te le dis pas souvent, tu auras pour toujours une très grande place dans mon coeur. Bonne année papa !",
        Emessage: "Daddy, despite the fact that I was not always a child you was expecting, you have been a patient and kind father.\n" +"My lovely daddy I wish you a Happy and joyful New Year ",
        type: "Nouvelan"
    },
    {
        number: 9,
        Fmessage: "Une petite carte pour te rendre hommage, mon cher père. Sache que, même si je ne te le dis pas souvent, tu auras pour toujours une très grande place dans mon coeur.",
        Emessage: " I love you Daddy " + " I wish you a Happy and prosperous New Year",
        type: "Nouvelan"
    },
    {
        number: 10,
        Fmessage: "",
        Emessage: "Happy New Year Daddy all the best wishes for the year to start... This end of year is a new opportunity to express my gratitude for all the your care",
        type: "Nouvelan"
    },
    {
        number: 11,
        Fmessage: "",

        Emessage: "I love you my dear father, you are one of the best and caring man i ever seen... For this new year i wish you all the luck and peace... May the Lord almighty grant you more strength",
        type: "Nouvelan"

    },
    {
        number: 12,
        Fmessage: "",
        Emessage: "My dear father for this new year, i wish to thank you for all you have being doing for me. Happy New Year",
        type: "Nouvelan "
    },
    {
        number: 13,
        Fmessage: "",
        Emessage: "Happy new year Daddy. May the lord grant you all what you desire this year ",
        type: "Nouvelan "
    },
    {
        number: 14,
        Fmessage: "",
        Emessage: "Merry Christmas to you Daddy",
        type: "Noel "
    },
    {
        number: 15,
        Fmessage: "",
        Emessage: "Have a great Christmas!"+"Wishing you Happiness for the upcomng year",
        type: "Noel "
    },
    {
        number: 16,
        Fmessage: "",
        Emessage: "I wish you a special Joy on this Christmas night!",
        type: "Noel "
    },
    {
        number: 17,
        Fmessage: "",
        Emessage: "May your Christmas be white and bright! ... A warm and happy Christmas for you Dad",
        type: "Noel "
    },
];
var maman = [
    {
        number: 1,
        Fmessage: " Joyeux Noël Maman et Agréables fêtes de fin d'année à la meilleure des mères du monde. Je t'aime d'un amour unique Ma douce Mère!",
        Emessage: " My dear mummy, I wish you a Happy Christmas From your lovely child",
        type: "Noel"
    },
    {
        number: 2,
        Fmessage: "Petite maman, les fêtes de fins d'année nous rappellent l'amour de ceux que l'on aime... " +
            "Je voudrais te souhaiter un Joyeux Noël Maman et te présenter mes voeux de Bonne Année avec tendresse et beaucoup de respect." +
            "Tu restes en ma vie le plus fort repère, celui qui me rend plus fort et fait que jamais je ne désespère, ...",
        Emessage: "My dear mummy for this end of year, I wish you a happy christmas and a wonderful end of year ",
        type: "Noel"
    },
    {
        number: 3,
        Fmessage: "Je t'aime ma mère, tu es la plus belle de toutes les mamans du monde..." +
            "Que le Nouvel An soit pour toi, ma maman chérie et pour mon papa chéri le début d'une nouvelle vie de tendresse, de bonheur... Que la santé vous accompagne durant toute l'année Nouvelle...",
        Emessage: "Happy Merry Christmas and happy end of year to the best Mum in the World" + 
                   "I love you my dear and caring mother",
        type: "Nouvelan"
    },
    {
        number: 4,
        Fmessage: "",
        Emessage: "Happy Merry Christmas and happy end of year to the best Mum in the World" + 
                   "I love you my dear and caring mother",
        type: "Noel"
    },
    {
        number: 5,
        Fmessage: "Meilleurs voeux à ma mère qui illumine ma vie de sa lumière\n" +
            "Mes voeux pour elle sont de toutes merveilles\n" +
            "Ma belle Maman Bonne Année et Joyeux Noël\n" +
            "Surtout ne change jamais, reste la même\n" +
            "Une maman magnifique que ses enfants aiment\n" +
            "Je t'aime maman...\n" +
            "Une fois de plus joyeux Noël et bonne année de la part d'un enfant à la meilleur des mères: ma Maman d'amour",
        Emessage: "",
        type: "Nouvelan"
    },
    {
        number: 6,
        Fmessage:"",
        Emessage: "For this year i wish you a happy and lovely new year and may God continue to give you health and strenght to guide me...Daddy all my best wishes...",
        type: "Nouvelan"
    }
];


var ami =  [
    {
        number: 1,
        Fmessage: "Comme un éternel renouvellement,\n" +
            "Cette année s’achève pour laisser place\n" +
            "À une page blanche que je te souhaite\n" +
            "De remplir des plus belles promesses…\n",
        Emessage: "message 2",
        type: "Nouvelan"
    },
    {
        number: 2,
        Fmessage: "De l’amour, des rêves et de belle surprises…je te souhaite une excellente nouvelle année !",
        Emessage: "message 2",
        type: "Nouvelan"
    },
        {
        number: 3,
        Fmessage: "Parce que notre amitié est précieuse,\n" +
            "Mes vœux de bonheur te sont dédiés,\n" +
            "À toi qui me rends si heureux/heureuse\n" +
            "Et qui es depuis tant d’années,\n" +
            "Une présence chaleureuse,\n" +
            "Douce et fidèle à mes côtés.\n" +
            "Joyeuses Fêtes",
        Emessage: "message 2",
        type: "Noel"
    },
            {
        number: 4,
        Fmessage: "Je te souhaite une excellente année 2019.\n" +
            "J’espère que tu continueras de fréquenter des gens talentueux et exceptionnel.\n" +
            "Alors, ne me perds pas de vue!",
        "Emessage": "message 2",
        type: "Nouvelan"
    },
            {
        number: 5,
        Fmessage: "   Je te souhaite pour cette nouvelle année de concrétiser tous tes grands projets\n" +

            "tes petites envies et de trouver encore\n" +
            "et toujours de nouvelles inspirations…" +
            "Bonne année!",
        Emessage: "message 2",
        type: "Nouvelan"
    }
];

  var   autre = [
        {
            number: 1,
            Fmessage: "Comme une belle étoile qui illumine le sapin de Noel, j’espère que votre vie sera illumine de bonheur, je vous écris cette carte pour vous souhaiter un joyeux Noël a vous et a toute votre famille.\n" +
                "Joyeux Noël!",
            Emessage: "message 2",
            type: "Noel"
        },
        {
            number: 2,
            Fmessage: "C’est avec le cœur rempli de belle pensée que je vous souhaite le plus merveilleux des Noëls.Que cette fête rempli votre cœur de bonheur et qu’elle vous apporte l’amour et la paix.Joyeux Noël !",
            Emessage: "message 2",
            type: "Noel"
        },
        {
            number: 3,
            Fmessage: "Joyeux Noël à vous et à votre famille, que cette douce nuit de Noël vous procurent les plus beaux des sensations: le bonheur d’être en famille, de l’amour et de la tendresse.",
            Emessage: "message 2",
            type: "Noel"
        },
        {
            number: 4,
            Fmessage: "Je te souhaite une excellente année 2019.\n" +
                "J’espère que tu continueras de fréquenter des gens talentueux et exceptionnel.\n" +
                " Alors, ne me perds pas de vue!",
            Emessage: "message 2",
            type: "Nouvelan"
        },
        {
            number: 5,
            Fmessage: "Je te souhaite pour cette nouvelle année de concrétiser tous tes grands projets\n" +

                "tes petites envies et de trouver encore\n " +
                "et toujours de nouvelles inspirations…\n" +

                "Bonne année!",
            Emessage: "message 2",
            type: "Nouvelan"
        }
    ];

     var    petitami =[
            {
                number: 1,
                Fmessage: "Pendant que la magie de Noël opère, mon coeur bat encore plus fort pour toi ",
                Emessage: "message 2",
                type: "Noel"
            },
            {
                number: 2,
                Fmessage: "Noël Brille de mille feux comme mon amour pour toi, mon amour…",
                Emessage: "message 2",
                type: "Noel"
            },
            {
                number: 3,
        Fmessage: "Je t’envoie un baisé plus sucré que le miel, plus blanc que le lait, plus doux que la cerise, mais pas mieux que toi.Bonne fête de Noel.je t’aime !",
                Emessage: "message 2",
                type: "Noel"
            },
        {
                number: 4,
                Fmessage: "À l’occasion du temps des fêtes, à Noel rien n’est plus agréable que de festoyer avec ceux qu’on aime.\n" +
                    "Joyeux Noël mon amour !",
                Emessage: "message 2",
                type: "Noel"
            },
        {
                number: 5,
                Fmessage: "En cette douce nuit de Noël je t’offre ma tendresse… je te souhaite joyeux Noel, tu es le plus beau des cadeaux de ma vie…",
                Emessage: "message 2",
                type: "Noel"
            },
        {
                number: 6,
                Fmessage: "Le nouvel an est un nouveau souffle pour notre amour ! Bonne année mon amour !!!",
                Emessage: "message 2",
                type: "Nouvelan"
            },
        {
                number: 7,
                Fmessage: "Bonne année 2019 à toi mon amour. Je te promets de remplir tes douze mois à venir d’amour et de tendresse. Gros bisous.",
                Emessage: "message 2",
                type: "Nouvelan"
            },
        {
                number: 8,
                Fmessage: "Voici l’aube du nouvel an et tant que je suis avec toi je suis sûr de lendemain plein de bonheur ! Bonne année 2019 !",
                Emessage: "message 2",
                type: "Nouvelan"
            },
        {
                number: 9,
                Fmessage: "Mon coeur,\n" +
                    " Pour 2019, je te souhaite plein de belle chose et beaucoup de bonheur auquel j’espère faire partie ! Bisous.",
                Emessage: "message 2",
                type: "Nouvelan"
            }
        ];

       var     petiteamie =  [
                {
                    number: 1,
                    Fmessage: "Pendant que la magie de Noël opère, mon coeur bat encore plus fort pour toi ",
                    Emessage: "message 2",
                    type: "Noel"
                },
                {
                    number: 2,
                    Fmessage: "Noël Brille de mille feux comme mon amour pour toi, mon amour…",
                    Emessage: "message 2",
                    type: "Noel"
                },
                {
                    number: 3,
                    Fmessage: "Je t’envoie un baisé plus sucré que le miel, plus blanc que le lait, plus doux que la cerise, mais pas mieux que toi. Bonne fête de Noel. je t’aime !",
                    Emessage: "message 2",
                    type: "Noel"
                },
        {
                    number: 4,
                    Fmessage: "À l’occasion du temps des fêtes, à Noel rien n’est plus agréable que de festoyer avec ceux qu’on aime.\n" +
                        "Joyeux Noël mon amour !",
                    Emessage: "message 2",
                    type: "Noel"
                },
        {
                    number: 5,
                    Fmessage: "Mon amour tu es la beauté et le sens de ma vie, tu es ma princesse de Noël… C’est avec toi que ce Noël va être joyeux…Que ce Noël soit rempli d’amour.",
                    Emessage: "message 2",
                    type: "Noel"
                },
        {
                    number: 6,
                    Fmessage: "Mon coeur Pour 2019, je te souhaite plein de belle chose et beaucoup de bonheur auquel j’espère faire partie ! Bisous.",
                    Emessage: "message 2",
                    type: "Nouvelan"
                },
        {
                    number: 8,
                    Fmessage: "Bonne année 2019 à toi mon amour. Je te promets de remplir tes douze mois à venir d’amour et de tendresse. Gros bisous.",
                    Emessage: "message 2",
                    type: "Nouvelan"
                },
        {
                    number: 9,
                    Fmessage: "Voici l’aube du nouvel an et tant que je suis avec toi je suis sûr de lendemain plein de bonheur ! Bonne année 2019 !",
                    Emessage: "message 2",
                    type: "Nouvelan"
                }
            ]
