var template = [
    {
        Fmessage: "Meilleurs voeux de bonheur à mon père que j'aime d'un amour éternel! Papa je te souhaite le meilleur de l'existence: l'amour de ta famille, l'amitié sincère de tes amis et surtout une bonne santé pour profiter pleinement des bonheurs quotidien! Ton fils qui t'aime avec respect et estime.",
        Emessage: "All my best wishes for this new upcoming year, I love you my dear Daddy",
        to: "A Mon super papa",
        bg: "00.jpg",
        sample: "00.jpg",
        logo:"88.png",
        Fwish:"Joyeux Noël et Bonne année 2019",
        Ewish:"Happy new year 2019",
        font : "f3",
        fontSize: 19,
        color: 'white',
    },

    {
        Fmessage: "Mon amour tu es la beauté et le sens de ma vie, tu es ma princesse de Noël… C’est avec toi que ce Noël va être joyeux…Que ce Noël soit rempli d’amour.",
        Emessage: "message 2",
        to: "A toi mon coeur",
        bg: "112.jpg",
        sample: "44.jpg",
        logo:"44.png",
        Fwish:"Joyeux Noël et Bonne année 2019",
        Ewish:"Happy new year 2019",
        font : "f9",
        fontSize: 28,
        color: 'black',
    },


    {
        Fmessage: " Joyeux Noël Maman et Agréables fêtes de fin d'année à la meilleure des mères du monde. Je t'aime d'un amour unique Ma douce Mère!",
        Emessage: " My dear mummy, I wish you a Happy Christmas From your lovely child",
        to: "A ma maman chérie",
        bg: "44.jpg",
        sample: "22.jpg",
        logo:"88.png",
        Fwish:"Joyeux noël et Bonn année 2019",
        Ewish:"Happy new year 2019",
        font : "f9",
        fontSize: 24,
        color: 'white',
    },

    {
        Fmessage: "Je t'aime mon tendre père, tu es le plus gentil et le plus tendre des papas du monde...  Que le nouvel an soit pour toi, mon papa chérie et pour ma maman chérie le début d'une nouvelle vie de tendresse, de bonheur... Que la santé vous accompagne durant toute l'année Nouvelle...",
        Emessage: "My dear Daddy, thanks for all the love and care, wishing you all the best for this new year",
        to: "A Mon super papa",
        bg: "55.jpg",
        sample: "11.jpg",
        logo:"55.png",
        Fwish:"Joyeux noël et Bonn année 2019",
        Ewish:"Happy new year 2019",
        font : "f9",
        fontSize: 19,
        color: 'black',
    },


    {
        Fmessage: "Je t’envoie un baisé plus sucré que le miel, plus blanc que le lait, plus doux que la cerise, mais pas mieux que toi.Bonne fête de Noel.je t’aime !",
        Emessage: "message 2",
        to: "A toi mon coeur",
        bg: "66.jpg",
        sample: "33.jpg",
        logo:"88.png",
        Fwish:"Joyeux Noël et Bonne année 2019",
        Ewish:"Happy new year 2019",
        font : "f9",
        fontSize: 29,
        color: 'black',
    },

]