<?php 
    session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8" />
  <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="css/fonts.css" />
  <link type="text/css" rel="stylesheet" href="css/snowstyle.css" />
  <link type="text/css" rel="stylesheet" href="css/style.css" />
  <link type="text/css" rel="stylesheet" href="css/bootstrap-colorpicker.min.css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://d1azc1qln24ryf.cloudfront.net/114779/Socicon/style-cf.css?u8vidh">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta property="og:url" content="http://wish-card.tk" />
  <meta property="og:type" content="image"/>
  <meta property="og:title" content="Wish-Card Generator" />
  <meta property="og:description" content="Générez et partagez vos cartes de voeux. Libérez votre créativité et faites plaisir à votre entourage." />
  <meta property="og:image" content="images/6.jpeg" />
  <title>Wish-Card Generator</title>
</head>

<body class="snowzone">
  <!-- <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1796093960691689',
        xfbml      : true,
        version    : 'v2.11'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script> -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

  <div class="pere_noel col m1 s1">
    <span>
      <img src="images/christmas3.gif" width="150px" alt="Father Christmas">
    </span>
  </div>

  <div class="slider col s12">
    <ul class="slides">
      <li>
        <img src="images/1.jpg" width="auto" height="250">
        <div class="caption center-align">
          <h2 style="color:#481341">Wish-Card</h2>
          <h3 class="light text-lighten-3">Simplifiez-vous la vie</h3>
          <a href="#card-model-chooser" class="scrolly btn rounded pulse">Commencer</a>
        </div>
      </li>
      <li>
        <img src="images/7.jpg" width="auto" height="250">
        <div class="caption left-align">
          <h2 style="color:#06c">Libérez votre créativité</h2>
          <h3 class="light lighten-3">Créez, personnalisez et partagez vos cartes de voeux</h3>
        </div>
      </li>
      <li>
        <img src="images/2.jpg" width="auto" height="250">
        <div class="caption right-align">
          <h2 style="color:#e28b9b">De nombreux modèles</h2>
          <h3 class="light pink-text lighten-3">Explorez notre grande varieté de modèles pour votre carte</h3>
        </div>
      </li>
      <li>
        <img src="images/3.jpg" width="auto" height="250">
        <div class="caption left-align">
          <h2 style="color:#fff">Marquez vos proches</h2>
          <h3 class="light lighten-3">Montrez à vos proches que vous les appréciez</h3>
        </div>
      </li>
      <li>
        <img src="images/6.jpg" width="auto" height="250">
        <div class="caption center-align">
          <h2 style="color:#481341">Wish-Card</h2>
          <h3 class="light text-lighten-3">Une nouvelle expérience</h3>
          <a href="#card-model-chooser"  class="scrolly btn rounded pulse">Commencer</a>
        </div>
      </li>
    </ul>
  </div>

  <div id="main">
    <div class="step-block" id="card-model-chooser">
      <div class="center">
        <h2>
          <span class="step-counter">1</span> Choisissez un modèle de carte de voeux ou
          <a href="#card-editor" id="begin" class="scrolly btn waves-effect waves-light">passez</a>
        </h2>
      </div>
      <section class="carousel">
        <div class="reel" id="samples">
          <!-- card's templates -->
        </div>
      </section>
    </div>



    <div class="row">
        <div class="step-block center col s12" id="card-editor">
          <h2>
            <span class="step-counter">2</span> Modifiez votre carte
          </h2>
        </div>
        <div class="col l5 m12 s12">
            <div class="col s12">
                <form>
                    <div class="input-field col s12 m6 l6">
                        <select id="to-chooser">
                            <option value="" disabled selected>Choisir une categorie</option>
                            <option value="">Sélectionnez svp</option>
                            <option value="maman">Maman</option>
                            <option value="papa">Papa</option>
                            <option value="petitami">Petit Ami</option>
                            <option value="petiteamie">Petite Amie</option>
                            <option value="ami">Ami(e)</option>
                            <option value="autre">Autre</option>
                        </select>
                        <label>Categorie</label>
                    </div>
                    <div class="input-field col s12 m6 l6">
                        <i class="mdi-action-account-circle prefix"></i>
                        <input id="to" name="to" type="text" class="validate" />
                    </div>
                    <div class="input-field col s12 m12">
                        <i class="mdi-image-dehaze prefix"></i>
                        <input id="title" name="title" type="text" class="validate" />
                        <label for="title">Titre de la carte</label>
                    </div>
                    <div class="input-field col s12 m12">
                        <i class="mdi-editor-mode-edit prefix"></i>
                        <textarea id="message" class="materialize-textarea" name="text"></textarea>
                        <a href="#!" id="refresh" class="offset-m5"> <i class="material-icons">autorenew</i> </a>
                        <label for="message">Message de votre carte </label> 
                    </div>
                    <div class="input-field col s12 m12">
                        <i class="mdi-image-dehaze prefix"></i>
                        <input id="wish" name="wish" type="text" class="validate" />
                        <label for="wish">Voeux</label>
                    </div>
                </form>
            </div>    
        </div>


        <div class="col m12 s12 l7">
            <div class="col s12" id="card-previewer">
                <div class="card-preview-container">
                  <div class="f3" id="wiltek">
                    <div class="card-preview-bg" id="card-bg">

                        <div class="card-preview-sq">
                        </div>
                        <!-- message's dest -->
                        <div class="card-preview-to" data-class="card-preview-to">
                        </div>
                        <!-- message's title -->
                        <div class="card-preview-title" data-class="card-preview-title">
                        </div>
                        <!-- message's title -->
                        <div class="card-preview-message" data-class="card-preview-message">
                        </div>
                        <!-- user's wish -->
                        <div class="card-preview-wish" data-class="card-preview-wish">
                        </div>
                        <div class="dev-signature hide"> powered by IAI-Dev </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="col s12 m12 l7 offset-l5">
            
            <div class="input-field col m4 s6">
              <select class="icons" id="bg">
                <option disabled selected></option>
                               
              </select>
              <label> choose your background </label>
            </div>

            <div class="input-field file-field col m4 s6">
              <div class="btn">
                <span><i class="large material-icons">file_upload</i></span>
                <input type="file" id="getbg">
              </div>
              <div class="file-path-wrapper">
                <input type="text" class=file-path placeholder="upload from device" validate>
              </div>
            </div>

            <div class="input-field col m4 s6">
              <select class="icons" id="sq">
                <option disabled selected></option>               
              </select>
              <label> choose your logo </label>
            </div>


            <div class="input-field col m4 s6 center">
              <select id="font-preview">
                  <!-- fonts -->
              </select>
              <label>Police</label>
            </div>

            <div id="color-picker-component" class="file-field col s6 m4">
            <label>Couleur</label>
            <div class="btn" style="float: right"></div>
            <div class="file-path-wrapper" style="padding-left: 0; padding-right: 10px;">
                <input type="text" id="card-text-color" />
            </div>
            </div>
            <div class="range-field col m4 s6 center">
            <label>Taille</label>
            <input type="range" id="font-size-range" value="18" min="0" max="100" />
            </div>
        </div>
      </div>



      
    <div class="input-field col s12">
        <center>
            <a href="#card-generator" id="card-generator-btn" class="btn-large btn waves-effect waves-light scrolly">GENERER</a>
        </center>
    </div>


    <div id="card-generator" class="step-block center">
      <div class="center">
        <h2>
          <span class="step-counter">4</span> Générez et partagez!
        </h2>
      </div>
      <div id="card-generator-preloader" class="preloader-wrapper big active hide">
        <div class="spinner-layer spinner-blue-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
      <div id="card-generated-image" class="hide">
        <div class="col s12">
          <img id="card-generated" style="max-width: 100%;" src="" class="card-generated" />
        </div>
        <div class="col s12">
          <ul class="icons">
            <li>
              <a id="link-share-facebook" target="_blank" href="#!" class="waves-effect waves-light circled socicon-facebook"></a>
            </li>
            <li>
              <a id="link-share-whatsapp" href="#!" data-action="share/whatsapp/share" class="waves-effect waves-light circled socicon-whatsapp"></a>
            </li>
          </ul>
        </div>
      </div>
      </div>
    </div>
  </div>

  <div id="footer" class="footer">
    <div class="container">
      <div class="row footer-block">
        <div class="col s6">
          <div class="col s12">
            <h1>Wish-Card Generator</h1>
          </div>
          <div class="col s12">
          </div>
        </div>
        <div class="col s6">
          <div class="col s12 title-block">
            <span class="title">Partager sur les réseaux sociaux</span>
          </div>
          <div class="col s12">
            <ul class="icons">
              <li>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwish-card.tk" class="waves-effect waves-light circled socicon-facebook"></a>
              </li>
              <li>
                <a href="whatsapp://send?text=http://wish-card.tk/'" data-action="share/whatsapp/share" class="waves-effect waves-light circled socicon-whatsapp"></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="copyright-block center col s12">
      <span class="copyright">&copy; Copyright 2018 - IAI Dev</span>
    </div>
  </div>

  
  <script type="text/javascript" src="js/snow.js"></script>
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/skel.min.js"></script>
  <script type="text/javascript" src="js/util.js"></script>
  <script type="text/javascript" src="js/jquery.dropotron.min.js"></script>
  <script type="text/javascript" src="js/jquery.scrolly.min.js"></script>
  <script type="text/javascript" src="js/jquery.onvisible.min.js"></script>
  <script type="text/javascript" src="js/bootstrap-colorpicker.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script type="text/javascript" src="js/templates.js"></script>
  <script type="text/javascript" src="js/voeux.js"></script>
  <script type="text/javascript" src="js/previewer.js"></script>
  <script type="text/javascript" src="js/messages.js"></script>
  
</body>

</html>